﻿using _mvc_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _mvc_.Controllers
{
    public class EstudiantesDocentesController : Controller
    {
        // Metodo Para retornar personas 
        
        
        public ActionResult Index()
        {
            var Personas = from y in RetornoPersona()
                           orderby y.Tabla
                           select y;
            return View(Personas); // RETORNA HACIA LA VISTA
        }

        // GET: EstudiantesDocentes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EstudiantesDocentes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstudiantesDocentes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudiantesDocentes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EstudiantesDocentes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudiantesDocentes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EstudiantesDocentes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [NonAction]// Para que no realice peticiones desde la vista.
        
        
        
        // Metodo-Lista de personas 
        public List<Persona> RetornoPersona()
        {
            return new List<Persona>
                {
                    new Persona
                    {
                        Tabla=1,
                        Nombre= "JULIO",
                        Apellidos = "ESPINOZA",
                        Edad=15,
                        Año="10mo",
                        rol="Estudiante",
                        Fechadeingreso=DateTime.Parse(DateTime.Today.ToString()),


                    },
                     new Persona
                    {
                        Tabla=2,
                        Nombre= "RODOLFO",
                        Apellidos = "MENDOZA",
                        Edad=30,
                        Año="10mo",
                        rol="Docente",
                        Fechadeingreso=DateTime.Parse(DateTime.Today.ToString()),

                    },
                      new Persona
                    {
                        Tabla=3,
                        Nombre= "MARIO",
                        Apellidos = "TRIANA",
                        Edad=40,
                        Año="8vo",
                        rol="Docente",
                        Fechadeingreso=DateTime.Parse(DateTime.Today.ToString()),

                    },
                       new Persona
                    {
                        Tabla=4,
                        Nombre= "WILMER",
                        Apellidos = "PALACIOS",
                        Edad=15,
                        Año="9no",
                        rol="Estudiante",
                        Fechadeingreso=DateTime.Parse(DateTime.Today.ToString()),
                      
                    }

                };
        }
    }
}
