﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _mvc_.Models
{
   // Propiedades de la persona 
    public class Persona
    {
        public int Tabla { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public int Edad { get; set; }
        public string Año { get; set; }
        public  string rol { get; set; }
        public DateTime Fechadeingreso { get; set; }
       
    }
}